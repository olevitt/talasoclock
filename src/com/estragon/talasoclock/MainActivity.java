package com.estragon.talasoclock;

import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.estragon.talasoclock.CountDown.ITimer;

public class MainActivity extends Activity implements OnInitListener, OnTouchListener, ITimer {

	public static TextToSpeech talker;
	CountDown countDown;
	static int START_TIME = 60+8;
	public static int time = Integer.MAX_VALUE;
	CountDownTimer timer = null;
	MediaPlayer mPlayer;
	
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		countDown = (CountDown) findViewById(R.id.countdown);
		countDown.setTimer(this);
		countDown.setOnTouchListener(this);
		talker  = new TextToSpeech(this, this);
		restart();
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}
	
	public void restart() {
		if (time == Integer.MAX_VALUE) {
			time = START_TIME;
		}
		if (timer != null) {
			timer.cancel();
		}
		timer = new CountDownTimer((time+1)*1000,1000) {

			
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				time--;
				if (time < 20 && time > 0) {
					talker.speak(""+time+" !!!", TextToSpeech.QUEUE_ADD, new HashMap<String, String>());
				}
				if (time % 60 == 0 && time > 0) {
					speakProchaineVague();
				}
				if (time == 0) {
					if (mPlayer == null)
						mPlayer = MediaPlayer.create(MainActivity.this, R.raw.alert);
					mPlayer.seekTo(0);
					mPlayer.start();
				}
				countDown.invalidate();
			}

			
			public void onFinish() {
				// TODO Auto-generated method stub
				time = Integer.MAX_VALUE;
				restart();
			}
		};
		timer.start();
	}
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			showDialog(1);
		}
		return false;
	}
	
	

	
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		CustomDialog dialog = new CustomDialog(this);
		return dialog;
	}
	
	public class CustomDialog extends Dialog {

		EditText minutes = null;
		Button boutonPlus, boutonMoins, valider;
		
		public CustomDialog(Context context) {
			super(context);
			setContentView(R.layout.dialog);
			setTitle("Durée (minutes)");
			setOnDismissListener(new OnDismissListener() {
				
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					
				}
			});
			minutes = (EditText) findViewById(R.id.minutes);
			minutes.setEnabled(false);
			boutonPlus = (Button) findViewById(R.id.boutonPlus);
			boutonPlus.setOnClickListener(new View.OnClickListener() {
				
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boutonPlus();
				}
			});
			boutonMoins = (Button) findViewById(R.id.boutonMoins);
			boutonMoins.setOnClickListener(new View.OnClickListener() {
				
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boutonMoins();
				}
			});
			
			valider = (Button) findViewById(R.id.valider);
			valider.setOnClickListener(new View.OnClickListener() {
				
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity.START_TIME = Integer.parseInt(minutes.getText().toString()) * 60;
					time = Integer.MAX_VALUE;
					restart();
				}
			});
		}
		
		public void boutonPlus() {
			modifierMinutes(1);
		}
		
		public void boutonMoins() {
			modifierMinutes(-1);
		}
		
		public void modifierMinutes(int nb) {
			
			int ancienMinutes = Integer.parseInt(minutes.getText()+"");
			if (ancienMinutes + nb < 1) {
				minutes.setText("1");
			}
			else {
				minutes.setText((ancienMinutes+nb)+"");
			}
		}
		
	}

	public static void speakProchaineVague() {
		int secondes = time%60;
		int minutes = time/60;
		if (secondes == 0 && minutes == 0) return;
		if (secondes == 0) 
			talker.speak("Prochaine vague dans "+(time/60)+" minuttes", TextToSpeech.QUEUE_FLUSH, new HashMap<String, String>());
		else if (minutes == 0) 
			talker.speak("Prochaine vague dans "+(time%60)+" secondes !", TextToSpeech.QUEUE_FLUSH, new HashMap<String, String>());
		else 
			talker.speak("Prochaine vague dans "+(time/60)+" minuttes, "+(time%60)+" secondes !", TextToSpeech.QUEUE_FLUSH, new HashMap<String, String>());
	}
	

	
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (timer != null)
		timer.cancel();
		timer = null;
		super.onDestroy();
	}



	
	public void onInit(int status) {
		// TODO Auto-generated method stub
		talker.setLanguage(Locale.FRANCE);
		talker.setSpeechRate(0.9f);
		talker.setPitch(0.3f);
	}

	
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}



	
	public int getTime() {
		// TODO Auto-generated method stub
		return time;
	}


}
