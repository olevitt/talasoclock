package com.estragon.talasoclock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.View;

public class CountDown extends View {


	
	Paint paint = new Paint();
	ITimer timer;
	

	public CountDown(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	public CountDown(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public CountDown(Context context) {
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}



	public void init() {
		paint.setTextSize(480);
		paint.setColor(Color.RED);
	}

	public void setTimer(ITimer timer) {
		this.timer = timer;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		canvas.drawColor(Color.BLACK);
		if (timer == null) return;
		PointF p = getTextCenterToDraw(getText(), new RectF(0, 0, getWidth(), getHeight()), paint);
		canvas.drawText(getText(), p.x, p.y, paint);
	}

	public static PointF getTextCenterToDraw(String text, RectF region, Paint paint) {
		Rect textBounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), textBounds);
		float x = region.centerX() - textBounds.width() * 0.5f;
		float y = region.centerY() + textBounds.height() * 0.5f;
		return new PointF(x, y);
	}

	public String getMinutes(boolean reel) {
		int minutes = timer.getTime() / 60;
		if (reel) return ""+minutes;
		if (minutes < 10) {
			return "0"+minutes;
		}
		return ""+minutes;
	}

	public String getSeconds(boolean reel) {
		int secondes = timer.getTime() % 60;
		if (reel) return ""+secondes;
		if (secondes < 10) {
			return "0"+secondes;
		}
		return ""+secondes;
	}

	public String getText() {
		return getMinutes(false)+":"+getSeconds(false);
	}


	public interface ITimer {
		public int getTime();
	}

}
