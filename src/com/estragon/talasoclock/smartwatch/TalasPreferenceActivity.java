package com.estragon.talasoclock.smartwatch;

import com.estragon.talasoclock.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class TalasPreferenceActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.preferences);
	}

	
}
